/* AudioStreamOutALSA.cpp
 **
 ** Copyright 2008-2009 Wind River Systems
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **
 **     http://www.apache.org/licenses/LICENSE-2.0
 **
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 */

#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>

#define LOG_TAG "AudioHardwareALSA"
#include <utils/Log.h>
#include <utils/String8.h>

#include <cutils/properties.h>
#include <media/AudioRecord.h>
#include <hardware_legacy/power.h>

#include "AudioHardwareALSA.h"

#ifndef ALSA_DEFAULT_SAMPLE_RATE
#define ALSA_DEFAULT_SAMPLE_RATE 44100 // in Hz
#endif

namespace android_audio_legacy
{

// ----------------------------------------------------------------------------

static const int DEFAULT_SAMPLE_RATE = ALSA_DEFAULT_SAMPLE_RATE;

// ----------------------------------------------------------------------------

AudioStreamOutALSA::AudioStreamOutALSA(AudioHardwareALSA *parent, alsa_handle_t *handle) :
    ALSAStreamOps(parent, handle),
    mFrameCount(0),
    mbitstreambuffer(NULL),
    mBitstreamMode(false),
    mIsHbr(false)
{
#ifdef ALSA_LRSWITCH
	mChannelMode = E_STEREO;
#endif
}

AudioStreamOutALSA::~AudioStreamOutALSA()
{
	LOGI("~AudioStreamOutALSA");
	if(mbitstreambuffer)
	    free(mbitstreambuffer);

    close();
}

uint32_t AudioStreamOutALSA::channels() const
{
    int c = ALSAStreamOps::channels();
    return c;
}

status_t AudioStreamOutALSA::setVolume(float left, float right)
{
    return mixer()->setVolume (mHandle->curDev, left, right);
}

#ifdef ALSA_LRSWITCH
void AudioStreamOutALSA::setDtvOutputMode(int mode)
{
       LOGI("setDtvOutputMode : %#x", mode);
       //do not use mLock, because this will block a few seconds sometimes.
       //single thread allowed only.
       //android::AutoMutex lock(mLock);
       mChannelMode = mode;
}


#define LEFTCHANNELVALID       (0X0000ffff)
#define RIGHTCHANNELVALID      (0xffff0000)
//must acuqire AutoMutex mLock
void AudioStreamOutALSA::channel_valid(const void * buffer, size_t len, int channelmode)
{

       //int *PpcmData = (int*)buffer;
       //int *buffer_end = (int*)buffer + (len/4);
       short *PpcmData = (short*)buffer;
	   short *buffer_end = (short*)buffer + (len/2);
       switch(channelmode){
               case E_LEFT:
                       while(PpcmData<buffer_end){
                               *(PpcmData+1) = *PpcmData;
                               PpcmData = PpcmData+2;
                       }

                       break;

               case E_RIGHT:
                       while(PpcmData<buffer_end){
                               *PpcmData = *(PpcmData+1);
                               PpcmData = PpcmData+2;
                       }

                       break;

               case E_STEREO:
               case E_SURROUND:
                       //DO NOTHING
                       break;

               default:
                       LOGE("channel_valid: unknown err.");

       }
}
#endif

#ifdef ALSA_VOLGAIN
void volumeGain(const void * buffer, size_t len, int db)
{
	short * buffer_end = (short*)buffer + (len/2);
	short * pcmData = (short *)buffer;
	int tmp;
	while(pcmData<buffer_end){
		tmp = (int)(*pcmData)<<db;
		*pcmData = tmp > 32767 ? 32767 : (tmp < -32768 ? -32768 : tmp);
		++pcmData;
	}
}
#endif

//need lock
ssize_t AudioStreamOutALSA::write_l(snd_pcm_t *handle, const void *buffer, size_t bytes)
{
    snd_pcm_sframes_t n;
    size_t            sent = 0;

    if(handle == NULL){
		ALOGI("write_l: handle is null.");
		return NO_INIT;
    }

    do {
        n = snd_pcm_writei(handle,
                           (char *)buffer + sent,
                           snd_pcm_bytes_to_frames(handle, bytes - sent));

        if (n == -EBADFD) {
            // Somehow the stream is in a bad state. The driver probably
            // has a bug and snd_pcm_recover() doesn't seem to handle this.
            LOGE("AudioStreamOutALSA snd_pcm_writei return -EBADFD");
            //mHandle->module->open(mHandle, mHandle->curDev, mHandle->curMode);
            return n;

        }
        else if (n < 0) {
			LOGE("AudioStreamOutALSA snd_pcm_writei return = %ld\n",n);
            if (handle) {
                // snd_pcm_recover() will return 0 if successful in recovering from
                // an error, or -errno if the error was unrecoverable.
                if(-EIO==n || -EPIPE==n)
                {
                    return n;
                }
                n = snd_pcm_recover(handle, n, 1);
                if (n) return static_cast<ssize_t>(n);
			}	
        }
        else {
            mFrameCount += n;
            sent += static_cast<ssize_t>(snd_pcm_frames_to_bytes(handle, n));
        }

    } while (handle && sent < bytes);

	return sent;

}

static struct timeval cur_time, last_time;
static bool bfirst=true;

static int scount=0;
ssize_t AudioStreamOutALSA::write(const void *buffer, size_t bytes)
{

	status_t status = NO_INIT;
	size_t sent = 0;
	int temp;
	int p;
	int i=0;

#ifdef ALSA_LRSWITCH
    if(mParent->mDirectMode != 1){
		channel_valid(buffer, bytes, mChannelMode);
    }
#endif 

#ifdef ALSA_VOLGAIN
    volumeGain(buffer, bytes, 2);
#endif

    size_t newbytes = (bytes*3)/2;
    if(mBitstreamMode){
        if(!mbitstreambuffer){
            LOGI("new mbitstreambuffer\n");
            mbitstreambuffer = (char*)malloc(newbytes);
        }

        char *ptr = (char*)buffer;
        char *ptr_end = (char*)buffer+bytes;
        char *newptr = (char*)mbitstreambuffer;
        memset(mbitstreambuffer, 0x0, newbytes);
        //LOGI("newbytes: %d, bytes: %d, scount: %d\n", newbytes, bytes, scount);
        while(ptr<ptr_end){
        #if 0
            newptr[1] = ptr[0];
            newptr[2] = ptr[1];
        #else
            newptr[0] = (ptr[0]&0x1f)<<3;
            newptr[1] = ((ptr[0]&0xe0)>>5)|((ptr[1]&0x1f)<<3);
            newptr[2] = (ptr[1]&0xe0)>>5;

            newptr[2] |= mParent->mChnsta[scount];

            temp = (newptr[2]<<24) | (newptr[1]<<16) | (newptr[0]<<8);
            i=0;
            p=0;
            while(i<31) {
                p ^= temp&0x1;
                p &= 0x1;
                temp >>= 1;
                i++;
            }
            newptr[2] |= (p&0x01)<<6;
        #endif
            scount++;
            scount %= 384;
            ptr +=2;
            newptr +=3;
        }
    }

#if 0
	gettimeofday(&cur_time, NULL);
	if(bfirst){
	    gettimeofday(&last_time, NULL);
        bfirst =false;
	}
	int delta = (cur_time.tv_sec  - last_time.tv_sec ) * 1000 +
		(cur_time.tv_usec - last_time.tv_usec) / 1000;
	ALOGE("write interval %ld ms", delta);
	if(delta >= 180)
		ALOGE("..................................\n");
	last_time = cur_time;
#endif
	char value[PROPERTY_VALUE_MAX] = "";
	property_get("media.cfg.audio.direct",value,"false");
	if(memcmp(value, "true", 4) == 0) {
		mParent->mXBMCDirectMode = 1;
		//ALOGD("Enter Hardware write,get media.cfg.audio.direct is true mHandle->devices = 0x%x", mHandle->devices);
		if (mHandle->devices != (AudioSystem::DEVICE_OUT_AUX_DIGITAL
					 | AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET)){
			//ALOGD("The current handle is XBMC ui, so standby the ui audio handle!");
			standby();
			usleep((((bytes * 1000) / 4) * 1000) / 44100);
			return bytes;
		}
	} else {
		mParent->mXBMCDirectMode = 0;
	}

    do {
        android::AutoMutex lock(mLock);//do{}while(0) excute once.

        if(mParent->mAudioState ==0)
            mParent->mAudioState=1;

        if (!mPowerLock) {
            acquire_wake_lock (PARTIAL_WAKE_LOCK, "AudioOutLock");
            mPowerLock = true;
            LOGI("Audio exiting sandby will open audio device");
            //LOGD("curDev =%d,, curMode =%d",mHandle->curDev, mHandle->curMode);
            if(mCurDev == 0)
                LOGE("unexperted error, need to be fixed!!!");
            if(!mHandle->handle)
                mHandle->module->open(mHandle, mCurDev/*mHandle->curDev*/, mHandle->curMode,
                	(mParent->mDirectMode? HW_PARAMS_FLAG_NLPCM : HW_PARAMS_FLAG_LPCM));
        }

        if(mHandle->handle == NULL)
            break;

        if(mBitstreamMode){
            sent = write_l(mHandle->handle, mbitstreambuffer, newbytes);
            if(sent != newbytes){
                break;
            }
        }else{
            sent = write_l(mHandle->handle, buffer, bytes);
            if(sent != bytes){
                break;
            }
        }
		if(!mHandle->next) {
			return bytes;
		}

		if(mHandle->next->handle){
			sent = write_l(mHandle->next->handle, buffer, bytes);
			if(sent != bytes)
				break;
		}

        return bytes;

    } while(0);

    // Simulate audio output timing in case of error
    LOGW("god says: keep silence");
    standby();
    usleep((((bytes * 1000) / 4) * 1000) / 44100);
    return bytes;
}

status_t AudioStreamOutALSA::dump(int fd, const Vector<String16>& args)
{
    return NO_ERROR;
}

status_t AudioStreamOutALSA::open(int mode)
{
    android::AutoMutex lock(mLock);
	return mHandle->module->route(mHandle, mHandle->curDev, mHandle->curMode,
	                              (mParent->mDirectMode? HW_PARAMS_FLAG_NLPCM : HW_PARAMS_FLAG_LPCM));
//	return ALSAStreamOps::open(mode);
}

status_t AudioStreamOutALSA::close()
{
	LOGE("AudioStreamOutALSA::close");
    android::AutoMutex lock(mLock);
    //reset samplerate channels after DirectOutput closed
    mHandle->sampleRate = 44100;
    mHandle->channels = 2;
    mHandle->format = SND_PCM_FORMAT_S16_LE;
    //if(mHandle->handle)
      //  snd_pcm_drain (mHandle->handle);
    ALSAStreamOps::close();
	//reset prop value
	property_set("media.cfg.audio.soundeffect", "true");
    if (mPowerLock) {
        release_wake_lock ("AudioOutLock");
        mPowerLock = false;
    }
    mParent->mDirectMode = 0;
    return NO_ERROR;
}

status_t AudioStreamOutALSA::standby()
{
    status_t err = NO_ERROR;
	LOGD("AudioStreamOutALSA::standby().....");

	if(mHandle->curMode == AudioSystem::MODE_IN_CALL)
	{
		LOGD("incall mode cannot in standby");
		return NO_ERROR;
	}
	if(mParent->mXBMCDirectMode) {
		if (!mHandle->handle) {
			//ALOGD("The current is XBMC direct mode and standby UI handle is NULL,so return!");
			return NO_ERROR;
		}
	}

    android::AutoMutex lock(mLock);

    err = mHandle->module->standby(mHandle);
//     LOGD("AudioStreamOutALSA::standby curDev = %d, curMode = %d",mHandle->curDev, mHandle->curMode);

    if (mPowerLock) {
        release_wake_lock ("AudioOutLock");
        mPowerLock = false;
    }

    mFrameCount = 0;
    mParent->mAudioState=0;
    //mParent->mDirectMode = 0;
    return err;
}

#define USEC_TO_MSEC(x) ((x + 999) / 1000)

uint32_t AudioStreamOutALSA::latency() const
{
    // Android wants latency in milliseconds.
    return USEC_TO_MSEC (mHandle->latency);
}

// return the number of audio frames written by the audio dsp to DAC since
// the output has exited standby
status_t AudioStreamOutALSA::getRenderPosition(uint32_t *dspFrames)
{
    *dspFrames = mFrameCount;
    return NO_ERROR;
}

}       // namespace android
