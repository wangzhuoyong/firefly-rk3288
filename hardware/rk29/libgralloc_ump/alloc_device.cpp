/*
 * Copyright (C) 2010 ARM Limited. All rights reserved.
 *
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include <errno.h>
#include <pthread.h>

#include <cutils/log.h>
#include <cutils/atomic.h>
#include <hardware/hardware.h>
#include <hardware/gralloc.h>

#include <sys/ioctl.h>

#include "alloc_device.h"
#include "gralloc_priv.h"
#include "gralloc_helper.h"
#include "framebuffer_device.h"

#include "alloc_device_allocator_specific.h"
#include <cutils/properties.h>
#include <stdlib.h>


//zxl:for vpu info
#include "../libon2/vpu_global.h"

static int gralloc_alloc_framebuffer_locked(alloc_device_t* dev, size_t size, int usage, buffer_handle_t* pHandle)
{
	private_module_t* m = reinterpret_cast<private_module_t*>(dev->common.module);
    
	// allocate the framebuffer
	if (m->framebuffer == NULL)
	{
		// initialize the framebuffer, the framebuffer is mapped once and forever.
		int err = init_frame_buffer_locked(m);
		if (err < 0)
		{
			return err;
		}
	}

	const uint32_t bufferMask = m->bufferMask;
	const uint32_t numBuffers = m->numBuffers;
	const size_t bufferSize = m->finfo.line_length * m->info.yres;
	if (numBuffers == 1)
	{
		// If we have only one buffer, we never use page-flipping. Instead,
		// we return a regular buffer which will be memcpy'ed to the main
		// screen when post is called.
		int newUsage = (usage & ~GRALLOC_USAGE_HW_FB) | GRALLOC_USAGE_HW_2D;
		AERR( "fallback to single buffering. Virtual Y-res too small %d", m->info.yres );
		return alloc_backend_alloc(dev, bufferSize, newUsage, pHandle);
	}

	if (bufferMask >= ((1LU<<numBuffers)-1))
	{
		// We ran out of buffers.
		return -ENOMEM;
	}

	int vaddr = m->framebuffer->base;
	// find a free slot
	for (uint32_t i=0 ; i<numBuffers ; i++)
	{
		if ((bufferMask & (1LU<<i)) == 0)
		{
			m->bufferMask |= (1LU<<i);
			break;
		}
		vaddr += bufferSize;
	}

	// The entire framebuffer memory is already mapped, now create a buffer object for parts of this memory
	private_handle_t* hnd = new private_handle_t(private_handle_t::PRIV_FLAGS_FRAMEBUFFER, bufferSize, vaddr,
	                                             0, dup(m->framebuffer->fd), vaddr - m->framebuffer->base);
#ifdef GRALLOC_16_BITS
	/* match the framebuffer format */
	hnd->format = HAL_PIXEL_FORMAT_RGB_565;
#endif
	hnd->stride = m->finfo.line_length / (m->info.bits_per_pixel >> 3);
	hnd->byte_stride = m->finfo.line_length;

	/*
	 * Perform allocator specific actions. If these fail we fall back to a regular buffer
	 * which will be memcpy'ed to the main screen when fb_post is called.
	 */
	if (alloc_backend_alloc_framebuffer(m, hnd) == -1)
	{
		delete hnd;
		int newUsage = (usage & ~GRALLOC_USAGE_HW_FB) | GRALLOC_USAGE_HW_2D;
		AERR( "Fallback to single buffering. Unable to map framebuffer memory to handle:0x%x", (unsigned int)hnd );
		return alloc_backend_alloc(dev, bufferSize, newUsage, pHandle);
	}

	*pHandle = hnd;

	return 0;
}

static int gralloc_alloc_framebuffer(alloc_device_t* dev, size_t size, int usage, buffer_handle_t* pHandle)
{
	private_module_t* m = reinterpret_cast<private_module_t*>(dev->common.module);
	pthread_mutex_lock(&m->lock);
	int err = gralloc_alloc_framebuffer_locked(dev, size, usage, pHandle);
	pthread_mutex_unlock(&m->lock);
	return err;
}

static int alloc_device_alloc(alloc_device_t* dev, int w, int h, int format, int usage, buffer_handle_t* pHandle, int* pStride)
{
	if (!pHandle || !pStride)
	{
		return -EINVAL;
	}

	size_t size;
	size_t stride;
	int    byte_stride;
	if ( format == HAL_PIXEL_FORMAT_YCrCb_420_SP 
        || format == HAL_PIXEL_FORMAT_YV12 
        || format == HAL_PIXEL_FORMAT_YCrCb_NV12 
        || HAL_PIXEL_FORMAT_YCrCb_NV12_10 == format
        || format == HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO )
	{
        int bpp = 4;
        char property[PROPERTY_VALUE_MAX];
		int fmtflag = 0;
		switch (format)
		{
			case HAL_PIXEL_FORMAT_YCrCb_420_SP:
			case HAL_PIXEL_FORMAT_YV12:
				stride = GRALLOC_ALIGN(w, 16);
				byte_stride = stride;
				size = h * (stride + GRALLOC_ALIGN(stride/2,16));
				break;
				/*
				 * Additional custom formats can be added here.
				 */
			case HAL_PIXEL_FORMAT_YCrCb_NV12:
			case HAL_PIXEL_FORMAT_YCrCb_NV12_10: 
			case HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO:
				stride = GRALLOC_ALIGN(w, 16);
				byte_stride = stride;
#if GET_VPU_INTO_FROM_HEAD
                size = h * (stride * 2);
#else
				//zxl:add tVPU_FRAME at the end of allocated buffer
				if(stride >= 3840*4)
					size = h * (stride * 17/10) + sizeof(tVPU_FRAME);
				else
					size = h * (stride * 2) + sizeof(tVPU_FRAME);
#endif
				break;
			#if 0	
			case HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO:
				if (property_get("sys.yuv.rgb.format", property, NULL) > 0) {
					fmtflag = atoi(property);
				}
				if(fmtflag == 1)
					bpp = 2;
				else 
					bpp = 4;				
				stride = GRALLOC_ALIGN(w, 16);
				byte_stride = stride*bpp;
				size = h * byte_stride;
				break;
			#endif	
			default:
				return -EINVAL;
		}
	}
	else
	{
		int bpp = 0;
		switch (format)
		{
		case HAL_PIXEL_FORMAT_RGBA_8888:
		case HAL_PIXEL_FORMAT_RGBX_8888:
		case HAL_PIXEL_FORMAT_BGRA_8888:
		case HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED:
			bpp = 4;
			break;
		case HAL_PIXEL_FORMAT_RGB_888:
			bpp = 3;
			break;
		case HAL_PIXEL_FORMAT_RGB_565:
		//case HAL_PIXEL_FORMAT_RGBA_5551:
		//case HAL_PIXEL_FORMAT_RGBA_4444:
			bpp = 2;
			break;
			/*
			 * Additional custom formats can be added here.
			 */
		default:
			return -EINVAL;
		}
		size_t bpr = GRALLOC_ALIGN(w * bpp, 64);

#if 0
		// force odd 64 align to improve ddr utilization for mali-t760
		if(!(usage & GRALLOC_USAGE_HW_FB)) {
		    ALOGD("[%d x %d: %d]  bpr = %d -> %d", w, h, bpp, bpr, ODD_ALIGN(bpr, 64));
		    bpr = ODD_ALIGN(bpr, 64);
		}
#endif
		size = bpr * h;
		byte_stride = bpr;
		stride = bpr / bpp;
	}

	int err;

	if (usage & GRALLOC_USAGE_HW_FB)
	{
		err = gralloc_alloc_framebuffer(dev, size, usage, pHandle);
	}
	else
	{
		err = alloc_backend_alloc(dev, size, usage, pHandle);
	}

	if (err < 0)
	{
		return err;
	}

	private_handle_t *hnd = (private_handle_t *)*pHandle;
	hnd->width = w;
	hnd->height = h;
	hnd->video_width = 0;
	hnd->video_height = 0;
	if(!hnd->format)        hnd->format = format;
	if(!hnd->stride)        hnd->stride = stride;
	if(!hnd->byte_stride)   hnd->byte_stride = byte_stride;

	int private_usage = usage & (GRALLOC_USAGE_PRIVATE_0 |
	                             GRALLOC_USAGE_PRIVATE_1);
	switch (private_usage)
	{
	case 0:
		hnd->yuv_info = MALI_YUV_BT601_NARROW;
		break;
	case GRALLOC_USAGE_PRIVATE_1:
		hnd->yuv_info = MALI_YUV_BT601_WIDE;
		break;
	case GRALLOC_USAGE_PRIVATE_0:
		hnd->yuv_info = MALI_YUV_BT709_NARROW;
		break;
	case (GRALLOC_USAGE_PRIVATE_0 | GRALLOC_USAGE_PRIVATE_1):
		hnd->yuv_info = MALI_YUV_BT709_WIDE;
		break;
	}

	*pStride = hnd->stride;
	return 0;
}

static int alloc_device_free(alloc_device_t* dev, buffer_handle_t handle)
{
	if (private_handle_t::validate(handle) < 0)
	{
		return -EINVAL;
	}

	private_handle_t const* hnd = reinterpret_cast<private_handle_t const*>(handle);
	private_module_t* m = reinterpret_cast<private_module_t*>(dev->common.module);

	if (hnd->flags & private_handle_t::PRIV_FLAGS_FRAMEBUFFER)
	{
		// free this buffer
		private_module_t* m = reinterpret_cast<private_module_t*>(dev->common.module);
		const size_t bufferSize = m->finfo.line_length * m->info.yres;
		int index = (hnd->base - m->framebuffer->base) / bufferSize;
		m->bufferMask &= ~(1<<index); 
		close(hnd->fd);
	}

	alloc_backend_alloc_free(hnd, m);

	delete hnd;

	return 0;
}

int alloc_device_open(hw_module_t const* module, const char* name, hw_device_t** device)
{
	alloc_device_t *dev;
	
	dev = new alloc_device_t;
	if (NULL == dev)
	{
		return -1;
	}

	/* initialize our state here */
	memset(dev, 0, sizeof(*dev));

	/* initialize the procs */
	dev->common.tag = HARDWARE_DEVICE_TAG;
	dev->common.version = 0;
	dev->common.module = const_cast<hw_module_t*>(module);
	dev->common.close = alloc_backend_close;
	dev->alloc = alloc_device_alloc;
	dev->free = alloc_device_free;

	if (0 != alloc_backend_open(dev)) {
		delete dev;
		return -1;
	}
	
	*device = &dev->common;

	return 0;
}
